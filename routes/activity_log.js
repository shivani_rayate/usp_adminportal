const express = require('express');
const router = express.Router();

const AWS = require('aws-sdk');
const cognito = require('../app/utilities/user_management/cognito');

const userActivity = require("../workers/userActivity");
const userActivityObj = new userActivity.userActivityClass();

router.get('/', function (req, res, next) {

    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('activity/activity_log', {  section: 'Activity Log', sub_section: '',     userData: userData, userName: userName, userRole: userRole });

})


// Get All Users
router.get('/listUser', (req, res, next) => {

  const params = {}

  cognito.getUsers(params).then((users) => {
    res.json({
      status: 200,
      message: 'Users fetched successfully',
      data: users,
    });
  });

});


router.post('/activity_log', (req, res, next) => {
 
  const params = {
    userName:req.body.Username
  }

userActivityObj.findUseractivityByUserName(params).then((data) => {
  res.json({
      status: 200,
      message: 'Metadata Loaded successfully',
      data: data,
  });
});
});



module.exports = router;