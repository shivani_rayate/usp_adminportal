const express = require('express');
const router = express.Router();

const AWS = require('aws-sdk');
const cognito = require('../app/utilities/tenant_management/cognito');

router.get('/userPoolListing', function (req, res, next) {

    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('admin/user_pool_listing', { section: 'User Pool Listing', sub_section: 'User', userData: userData, userName: userName, userRole: userRole });

})

router.get('/createUserPool', function (req, res, next) {

    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('admin/create_user_pool', { section: 'User Pool', sub_section: 'User', userData: userData, userName: userName, userRole: userRole });


})
router.post('/createUserPoolApp', (req, res, next) => {
    console.log("create user pool app POST called")
    const params = {
        poolName:req.body.poolName ? req.body.poolName: null,
        userPoolAddOns:req.body.userPoolAddOns ? req.body.userPoolAddOns:null,
       policies:req.body.policies ? req.body.policies:null,
       mfaConfiguration:req.body.mfaConfiguration? req.body.mfaConfiguration:null,
 }
  //console.log("create user pool app  > "+JSON.stringify(params))
    cognito.createUserPoolApp(params).then((response) => {
      if (response) {
        console.log('create user pool response >> ' + JSON.stringify(response));
        res.json({
          status: 200,
          message: 'create user pool Successfully !!!',
          data: response,
        })
        console.log(response.data);
      }
    }).catch((err) => {
      console.log(`error IN create user pool: ${err}`);
      res.json({
        status: 500,
        message: 'Oops ! Some error occured, please try again later.',
        data: null,
      });
    });
  
  })

//  list user pool

  router.get('/listUserPools', (req, res, next) => {
    console.log("list user pool POST called")
  
    const params = {
      maxResults: req.query.maxResults ? req.query.maxResults : null
    }
   console.log("maxResults data > "+JSON.stringify(params))
   cognito.listUserPools(params).then((response) => {
    if (response) {
      console.log(' list user pool  response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'list user pool Successfully',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})


// Describe user pool data

router.get('/describeUserPool', (req, res, next) => {
  console.log("Describe user pool Get called")

  const params = {
    userPoolId: req.query.userPoolId? req.query.userPoolId:null
  }
 console.log("maxResults data > "+JSON.stringify(params))
 cognito.describeUserPool(params).then((response) => {
  if (response) {
    console.log(' Describeuser pool  response >> ' + JSON.stringify(response));
    res.json({
      status: 200,
      message: 'Describe user pool Successfully',
      data: response,
    })
  }
}).catch((err) => {
  console.log(`error IN USER: ${err}`);
  res.json({
    status: 500,
    message: 'Oops ! Some error occured, please try again later.',
    data: null,
  });
 });
})



module.exports = router;