var express = require('express');
var router = express.Router();
const AWS = require('aws-sdk');

const Config = require("../workers/setting");
const settingObj = new Config.SettingClass();

const awsConfig = require('../config/aws_config')['development'];

AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});

// const mediaConvertUtil = require('../app/utilities/video/media_convert');

// ##############################################################################

let requestMethods = require("../app/utilities/commonRequestMethods.js");
let API = require('../config/api/api_list');


// ########## MICRO-1 ##########

let mcube_transcode_BASE = API.mcube_transcode.base.development;


router.get('/', function (req, res, next) {
    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('settings/configuration', { section: 'Settings', sub_section: 'Configuration', userData: userData, userName: userName, userRole: userRole });
});

/* POST : SAVE Configuration DETAILS. */
router.post('/', (req, res, next) => {
    console.log("configuration POST called")
    let params = {
        name: req.body.name,
        config: req.body.config ? req.body.config : null,
    }
    console.log("i am here>>>" + JSON.stringify(params))

    settingObj.save(params).then((_response) => {
        if (_response) {
            console.log('Configuration data saved');
            res.json({
                status: 200,
                message: 'Saved successfuly',
                data: _response,
            })

        } else {
            console.log('Configuration data save failed');
            res.json({
                status: 401,
                message: 'Configuration Save failed',
                data: null,
            })
        }
    })
})

// update Configuration
router.post('/updateConfig', (req, res, next) => {
    console.log("****** updateConfig called ****** ")
    let params = {
      url: mcube_transcode_BASE + API.mcube_transcode.settings.updateConfig,
      data:{
            name: req.body.name
      },
      headers:{
          token : req.session.userData.idToken.jwtToken
        }
  }
  console.log(" update config in UI  >>> " +JSON.stringify(params))
  requestMethods.postApiRequestWithHeader(params).then((_response) => {
        res.json(_response);
    });
})

module.exports = router;