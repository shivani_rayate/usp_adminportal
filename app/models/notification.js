const mongoose = require('mongoose');
const moment = require('moment');

const Schema = mongoose.Schema;

const NotificationSchema = new Schema({
    name: { type: String}, 
    description: { type: String},
    action: { type: String},
    created_at: Date,
    updated_at: Date,
    created_by: { type: String },
}, { toJSON: { virtuals: true } });

NotificationSchema.pre('save', function (next) {
    if (this.isNew) {
        console.log(' IS NEW CALLED!! ');
        this.created_at = new Date();
        this.updated_at = new Date();
        
    } else {
        console.log(' IS NEW IS FALSE!! ');
        this.updated_at = new Date();
    }
    next();
});


const Notification = mongoose.model('Notifications',NotificationSchema,'Notifications');

module.exports = Notification;