
function listUser() {
    return new Promise((resolve, reject) => {
        $.get('/v1/activity_log/listUser',
            function (data, status) {
                if (data.status == 200) {
                    resolve(data)
                    view(data)
                }
            });
    })
}
listUser()


$(document).on("click", ".activity_log", function (e) {
    var username = $("#view_source").val()

    let params = {
        Username: username
    }
    activity_log(params)
})


function view(data) {
    var array = data.data.Users;
    var userNamelist = '';
    array.forEach(function (element, i) {
        var Username = element.Username;
        userNamelist += `<option value="${Username}">${Username}</option>`;
    })
    $('#view_source').append(userNamelist);
}

function activity_log(params) {

    return new Promise((resolve, reject) => {
        $.post('/v1/activity_log/activity_log', params,
            function (data, status) {
                if (data.status == 200) {
                    resolve(data)
                    destroyRows()
                    appenduserActivityDatatable(data)
                }
            });
    })

}

// Append Backup Channel 
function appenduserActivityDatatable(data) {

    var array = data.data;
    var options_table = "";
    array.forEach(function (element, i) {

        var channel_Name = element.channelName ? element.channelName : "-";
        var modified_on = element.created_at ? moment(element.created_at).format('lll') : "-";
        var userName = element.userName ? element.userName : "-";
        var ip_address = element.ip_address ? element.ip_address : "-";
        var activity = element.activity ? element.activity : "-";

        options_table += `<tr class='channel-overview-tbl-row' data-channel-name=${channel_Name} data-data="${modified_on}" > <td class="index">${i + 1}</td>
        <td class="channel-name">${userName}</td>
          <td>${modified_on}</td>
          <td class="channel-name">${channel_Name}</td>
          <td>${activity}</td>
          <td>${ip_address}</td> 
               </tr>`

        if (i == array.length - 1) {
            $('#Userlisting_tbody').html(options_table)
            reInitializeDataTable()

        }
    })
}

var channel_listing_table;
function reInitializeDataTable() {
    return new Promise((resolve, reject) => {
        $("#user_listing_table").DataTable().destroy()
        channel_listing_table = $('#user_listing_table').DataTable({
            "order": [[ 0, "desc" ]]
        })
        resolve()
    })
}

function destroyRows() {
    $('#Userlisting_tbody').empty()
    $('#user_listing_table').DataTable().rows().remove();
    $("#user_listing_table").DataTable().destroy()
}

